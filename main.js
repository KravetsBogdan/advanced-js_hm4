let url = 'https://ajax.test-danit.com/api/swapi/films';

class Request{
    getUrl(){
       return fetch(url).then(response => response.json())
    }
}

const ul = document.querySelector('.films');
class Films{
    render(data){
        data.sort((a, b) => {
            return a.episodeId - b.episodeId
         })
        let filmList = data.map(({episodeId, name, openingCrawl, characters}) =>{
            let liName = document.createElement('li');

            let promises = characters.map((el) => {
                return fetch(el).then(response => response.json())
            })
            Promise.all(promises).then(data => {
                let hero = data.map(({name: heroName}) => {
                    liName.innerHTML = `
                    ID: ${episodeId}
                    <br>
                    Name: ${heroName},
                    <br>
                    Film name is: ${name}
                    <br>
                    Some info: ${openingCrawl}`;
                })
            })
            ul.append(liName);
        })
    }
}

const request = new Request;
const films = new Films;
request.getUrl().then(data => films.render(data)) 

